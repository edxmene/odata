﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Routing.Controllers;

namespace ODataWebAPI.Controllers
{
    public class CompanyController : ODataController
    {
        private BookStoreContext _db;
        public CompanyController(BookStoreContext context)
        {
            _db = context;
            if (context.Companies.Count() == 0)
            {
                foreach (var b in DataSource.GetCompanies())
                {
                    context.Companies.Add(b);
                    context.Drink.Add(b.specialDrink);
                }
                context.SaveChanges();
            }
        }

        // Return all companies
        [EnableQuery]
        public IActionResult Get()
        {
            return Ok(_db.Companies);
        }

        [EnableQuery]
        public IActionResult Get(int key)
        {
            return Ok(_db.Companies.FirstOrDefault(c => c.Id == key));
        }

        [EnableQuery]
        public IActionResult Post([FromBody] Company company)
        {
            _db.Companies.Add(company);
            _db.SaveChanges();
            return Created(company);
        }
    }
}
