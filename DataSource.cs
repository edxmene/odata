﻿namespace ODataWebAPI
{
    public static class DataSource
    {
        private static IList<Book> _books { get; set; }
        private static IList<Company> _companies { get; set; }

        public static IList<Book> GetBooks()
        {
            if (_books != null)
            {
                return _books;
            }

            _books = new List<Book>();

            // book #1
            Book book = new Book
            {
                Id = 1,
                ISBN = "978-0-321-87758-1",
                Title = "Essential C#5.0",
                Author = "Mark Michaelis",
                Price = 59.99m,
                Location = new Address { City = "Redmond", Street = "156TH AVE NE" },
                Press = new Press
                {
                    Id = 1,
                    Name = "Addison-Wesley",
                    Category = Category.Book,
                    Email = "a@gmail.com"
                }
            };
            _books.Add(book);

            // book #2
            book = new Book
            {
                Id = 2,
                ISBN = "063-6-920-02371-5",
                Title = "Enterprise Games",
                Author = "Michael Hugos",
                Price = 49.99m,
                Location = new Address { City = "Bellevue", Street = "Main ST" },
                Press = new Press
                {
                    Id = 2,
                    Name = "O'Reilly",
                    Category = Category.EBook,
                    Email = "o@gmail.com"
                }
            };
            _books.Add(book);

            return _books;
        }

        public static IList<Company> GetCompanies()
        {
            if (_companies != null)
            {
                return _companies;
            }

            _companies = new List<Company>();

            // company #1
            Company company = new Company
            {
                Id = 1,
                name = "Pepsi",
                rating = 9,
                specialDrink = new Drink
                {
                    Id = 1,
                    Name = "Pepsi zero",
                    Rating = 8
                }
            };
            _companies.Add(company);

            // company #2
            company = new Company
            {
                Id = 2,
                name = "Postobon",
                rating = 8,
                specialDrink = new Drink
                {
                    Id = 2,
                    Name = "Gaseosa Manzana",
                    Rating = 7
                }
            };
            _companies.Add(company);

            // company #3
            company = new Company
            {
                Id = 3,
                name = "Bavaria",
                rating = 9,
                specialDrink = new Drink
                {
                    Id = 3,
                    Name = "Aguila",
                    Rating = 8
                }
            };
            _companies.Add(company);

            company = new Company
            {
                Id = 4,
                name = "Budweiser",
                rating = 8,
                specialDrink = new Drink
                {
                    Id = 4,
                    Name = "Black",
                    Rating = 8
                }
            };
            _companies.Add(company);

            company = new Company
            {
                Id = 5,
                name = "Heineken",
                rating = 8,
                specialDrink = new Drink
                {
                    Id = 5,
                    Name = "Lager",
                    Rating = 7
                }
            };
            _companies.Add(company);

            company = new Company
            {
                Id = 6,
                name = "Ajecolombia",
                rating = 3,
                specialDrink = new Drink
                {
                    Id = 6,
                    Name = "BigCola",
                    Rating = 8
                }

            };
            _companies.Add(company);

            company = new Company
            {
                Id = 7,
                name = "Cerveceria Union",
                rating = 4,
                specialDrink = new Drink
                {
                    Id = 7,
                    Name = "Malta",
                    Rating = 5
                }
            };
            _companies.Add(company);

            company = new Company
            {
                Id = 8,
                name = "Gaseosa Lux",
                rating = 5,
                specialDrink = new Drink
                {
                    Id = 8,
                    Name = "Soda",
                    Rating = 6
                }

            };
            _companies.Add(company);

            return _companies;
        }
    }
}
