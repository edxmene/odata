﻿
using Microsoft.EntityFrameworkCore;

namespace ODataWebAPI
{
    public class BookStoreContext : DbContext
    {
        public BookStoreContext(DbContextOptions<BookStoreContext> options)
            : base(options)
        {
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Press> Presses { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Drink> Drink { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>().OwnsOne(c => c.Location);
            //modelBuilder.Entity<Company>().OwnsOne(c => c.name);
        }
    }
}
